$(document).ready(function(){$('.carousel').carousel({interval:false});

/* affix the navbar after scroll below header */
$('#nav').affix({
      offset: {
        top: $('header').height()-$('#nav').height()
      }
});	

/* highlight the top nav as scrolling occurs */
$('body').scrollspy({ target: '#nav' })

/* smooth scrolling for scroll to top */
$('.scroll-top').click(function(){
  $('body,html').animate({scrollTop:0},1000);
})

/* Ocultar botón scroll-top cuando no se ha scrolleado todavía. */
var verifyScroll = function() {
  if($(window).scrollTop() > $(window).height()) {
    $('.scroll-top').show();
  } else {
    $('.scroll-top').hide();
  }
};
verifyScroll(); // Primera ejecución para definir la visibilidad o no del botón.
$(window).on('scroll', verifyScroll); // Listener

/* smooth scrolling for nav sections */
$('#nav .navbar-nav li>a').click(function(){
  var link = $(this).attr('href');
  var posi = $(link).offset().top;
  $('body,html').animate({scrollTop:posi},700);
});


/* copy loaded thumbnails into carousel */
$('.panel .img-responsive').on('load', function() {
  
}).each(function(i) {
  if(this.complete) {
  	var item = $('<div class="item"></div>');
    var itemDiv = $(this).parent('a');
    var title = $(this).parent('a').attr("title");
    
    item.attr("title",title);
  	$(itemDiv.html()).appendTo(item);
  	item.appendTo('#modalCarousel .carousel-inner'); 
    if (i==0){ // set first item active
     item.addClass('active');
    }
  }
});

/* activate the carousel */
$('#modalCarousel').carousel({interval:false});

/* change modal title when slide changes */
$('#modalCarousel').on('slid.bs.carousel', function () {
  $('.modal-title').html($(this).find('.active').attr("title"));
})

/* when clicking a thumbnail */
$('.panel-thumbnail>a').click(function(e){
    e.preventDefault();
    var idx = $(this).parents('.panel').parent().index();
  	var id = parseInt(idx);
  	
  	$('#myModal').modal('show'); // show the modal
    $('#modalCarousel').carousel(id); // slide carousel to selected
  	return false;
});

/* Open Street Map */
  var map = new OpenLayers.Map("map-canvas");
  var address = $('#map-input').text();         // Dirección del evento

  map.addLayer(new OpenLayers.Layer.OSM());

  // Se crea el punto
  var lonLat = new OpenLayers.LonLat(-60.70711840000001, -31.6372312)
    .transform(new OpenLayers.Projection("EPSG:4326")
      , new OpenLayers.Projection("EPSG:900913")
  );  
  
  // Se crea el marker
  var markers = new OpenLayers.Layer.Markers( "Markers" );
  
  // Se crea el popup
  var popup = new OpenLayers.Popup.FramedCloud("Popup", 
      lonLat, null, address, null,
      false // <-- true si queremos el botón cerrar (X), sino false.
  );
  
  map.setCenter(lonLat, 15);
  map.addLayer(markers);
  markers.addMarker(new OpenLayers.Marker(lonLat), address);
  map.addPopup(popup);
});