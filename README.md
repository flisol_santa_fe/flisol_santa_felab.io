Repositorio del sitio web para el FLISoL Santa Fe
=================================================

Info
----

- Forkeado de: https://gitlab.com/taulamet/flisol-propuesta-bootstrap
- Agiornado descargando ediciones previas de: flisolsantafe.org.ar
- Ahora alojado en flisol.lugli.org.ar

Cómo aportar contenido?
-----------------------

- Hacete un fork del repositorio (esto te va a crear un repo igual bajo tu usuario de gitlab)
- Creá un branch para hacer tus aportes
- Hacé tus aportes al repo
- Hacé un merge request para que incorporen tus cambios

